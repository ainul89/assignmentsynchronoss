package com.app.synchronoss.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.app.synchronoss.model.WeatherDataModel
import com.app.synchronoss.repository.WeatherRepositoryImpl
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class WeatherViewModel(private val retrofitAPIInterface: WeatherRepositoryImpl) : BaseViewModel() {
    private val weatherMutableLiveData = MutableLiveData<WeatherDataModel?>()
    fun getWeatherLiveData(): LiveData<WeatherDataModel?> {
        return weatherMutableLiveData
    }

    private fun isTimeExceeded(): Boolean {
        val time = retrofitAPIInterface.getWeatherTimeLastSync()
        return (System.currentTimeMillis() - time > (1000*60*60*2))
    }

    fun getWeatherList(lat: Double, lon: Double) {

        if(isTimeExceeded()) {
            val disposable = retrofitAPIInterface.getWeatherData(lat, lon)?.let {
                it.subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({ weatherDataModel ->
                            println("WeatherDataModel.accept = $weatherDataModel")
                            val gson = Gson()
                            val jsonString = gson.toJson(weatherDataModel)
                            retrofitAPIInterface.saveWeatherData(jsonString)
                            retrofitAPIInterface.saveWeatherTimeLastSync(System.currentTimeMillis())
                            weatherMutableLiveData.value = weatherDataModel
                        }) { throwable ->
                            println("WeatherDataModel.accept error = $throwable")
                            weatherMutableLiveData.value = null
                            // reschedule for next call after few minutes
                        }
            }
            compositeDisposable.add(disposable)
        } else {
            val data = retrofitAPIInterface.getSavedWeatherData()
            val gson = Gson()
            val weatherDataModel = gson.fromJson(data, WeatherDataModel::class.java)
            weatherMutableLiveData.value = weatherDataModel
        }
    }

    fun celciusToFahrenheit(temp: Double): Double {
            return (temp * 1.8f) + 32;
    }
}
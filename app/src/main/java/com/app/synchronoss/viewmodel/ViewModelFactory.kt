package com.app.synchronoss.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider.NewInstanceFactory
import com.app.synchronoss.repository.WeatherRepositoryImpl

/**
 * General view model factory for all view models
 */
class ViewModelFactory(val application: Application) : NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return if (modelClass == WeatherViewModel::class.java) {
            return WeatherViewModel(WeatherRepositoryImpl(application)) as T
        } else {
            super.create(modelClass)
        }
    }
}
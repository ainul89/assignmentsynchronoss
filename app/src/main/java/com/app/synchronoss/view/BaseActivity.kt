package com.app.synchronoss.view

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import com.app.synchronoss.utility.Utility

abstract class BaseActivity<T : ViewModel> : AppCompatActivity() {
    protected lateinit var viewModel: T
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    val isNetworkAvailable: Boolean
        get() {
            if (Utility.isNetworkConnected(this)) {
                return true
            }
            Toast.makeText(this, "No internet available!", Toast.LENGTH_SHORT).show()
            return false
        }
}
package com.app.synchronoss.view

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.work.*
import com.app.synchronoss.R
import com.app.synchronoss.model.WeatherDataModel
import com.app.synchronoss.utility.Utility.formatToServerTimeDefaults
import com.app.synchronoss.utility.Utility.formatToTwoDecimal
import com.app.synchronoss.viewmodel.ViewModelFactory
import com.app.synchronoss.viewmodel.WeatherViewModel
import com.app.synchronoss.worker.WeatherWorker
import com.google.android.gms.location.*
import com.google.android.gms.tasks.OnCompleteListener
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import java.util.concurrent.TimeUnit


private const val WORKER_TAG = "WORKER_TAG"
class WeatherActivity : BaseActivity<WeatherViewModel>() {
private val PERMISSION_ID = 10;
    lateinit var mFusedLocationClient: FusedLocationProviderClient
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewModel = ViewModelProviders.of(this, ViewModelFactory(application)).get(WeatherViewModel::class.java)
        setObservers()
            val constraints = Constraints.Builder()
                    .setRequiredNetworkType(NetworkType.CONNECTED)
                    .build()
            val request = PeriodicWorkRequestBuilder<WeatherWorker>(2, TimeUnit.HOURS)
                    .setConstraints(constraints)
                    .build()
            WorkManager.getInstance(this).enqueueUniquePeriodicWork(
                    WORKER_TAG,
                    ExistingPeriodicWorkPolicy.KEEP,
                    request
            )

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        // method to get the location
        getLastLocation();

    }

    private fun setUI(weatherModel: WeatherDataModel) {
        val temp =(weatherModel.main.temp/10)
        temperature.text = (getString(R.string.temperature, temp.toString().formatToTwoDecimal(), viewModel.celciusToFahrenheit(temp)))
        windSpeed.text = weatherModel.wind.speed.toString()
        cloudiness.text = (weatherModel.clouds.all.toString() + "%")
        if (weatherModel.clouds.all > 35) {
            cloudsImage.setVisibility(View.VISIBLE)
        } else {
            cloudsImage.setVisibility(View.GONE)
        }

        sunset.text = "${Date(weatherModel.sys.sunrise).formatToServerTimeDefaults()}/${Date(weatherModel.sys.sunset).formatToServerTimeDefaults()}"
        place.text = "${weatherModel.name}, ${weatherModel.sys.country}"

    }

    private fun setObservers() {
        viewModel.getWeatherLiveData().observe(this, Observer { weatherDataModel ->
            weatherDataModel?.let {
                setUI(it)
            }
        })
    }

    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        // check if permissions are given
        if (checkPermissions()) {

            // check if location is enabled
            if (isLocationEnabled()) {

                // getting last
                // location from
                // FusedLocationClient
                // object
                mFusedLocationClient.getLastLocation().addOnCompleteListener(OnCompleteListener<Location?> { task ->
                    val location = task.result
                    if (location == null) {
                        requestNewLocationData()
                    } else {
                        latitude.text = location.latitude.toString() + "/" + location.longitude.toString()
                        viewModel.getWeatherList(location.latitude, location.longitude);
                    }
                })
            } else {
                Toast.makeText(this, "Please turn on" + " your location...", Toast.LENGTH_LONG).show()
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
        } else {
            // request for permissions
            requestPermissions()
        }
    }

    @SuppressLint("MissingPermission")
    private fun requestNewLocationData() {

        // Initializing LocationRequest object with appropriate methods
        val mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 5
        mLocationRequest.fastestInterval = 0
        mLocationRequest.numUpdates = 1

        // setting LocationRequest on FusedLocationClient
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper())
    }

    private val mLocationCallback: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            val mLastLocation = locationResult.lastLocation
            viewModel.getWeatherList(mLastLocation.latitude, mLastLocation.longitude)
            latitude.text = mLastLocation.latitude.toString() + "/" + mLastLocation.longitude.toString()
        }
    }

    // method to check for permissions
    private fun checkPermissions(): Boolean {
        return ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }

    // method to request for permissions
    private fun requestPermissions() {
        ActivityCompat.requestPermissions(this, arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION), PERMISSION_ID)
    }

    // method to check if location is enabled
    private fun isLocationEnabled(): Boolean {
        val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
    }

    // If everything is alright then
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_ID) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLastLocation()
            }
        }
    }

}
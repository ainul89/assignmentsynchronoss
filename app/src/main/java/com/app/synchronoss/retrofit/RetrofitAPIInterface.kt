package com.app.synchronoss.retrofit

import com.app.synchronoss.model.WeatherDataModel
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface RetrofitAPIInterface {

//    /data/2.5/weather?lat=35&lon=139&appid=0ae2c297304879bae1d091104c3cf5e2

    @GET("/data/2.5/weather")
    fun getCurrentWeather(
            @Query("lat") lat: Double,
            @Query("lon") lon: Double,
            @Query("appid") appId: String
    ): Observable<WeatherDataModel>
}
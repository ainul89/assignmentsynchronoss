package com.app.synchronoss.retrofit

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitConfig private constructor() {
    private val retrofit: Retrofit
    val gson: Gson

//    https://api.openweathermap.org/data/2.5/weather?lat=35&lon=139&appid=0ae2c297304879bae1d091104c3cf5e2
    private fun createRetrofit(): Retrofit {
        return Retrofit.Builder()
                .baseUrl("https://api.openweathermap.org/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClientBuilder.build()).build()
    }

    private val httpClientBuilder: OkHttpClient.Builder
        private get() = createHttpClient()

    private fun createHttpClient(): OkHttpClient.Builder {
        val httpClient = OkHttpClient.Builder()
        addInterceptors(httpClient)
        return httpClient
    }

    private fun addInterceptors(httpClient: OkHttpClient.Builder) {
        httpClient.readTimeout(30, TimeUnit.SECONDS)
        httpClient.connectTimeout(10, TimeUnit.SECONDS)
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.HEADERS
        logging.level = HttpLoggingInterceptor.Level.BODY
        httpClient.addInterceptor { chain ->
            val request = chain.request().newBuilder().addHeader("Content-Type", "application/json").build()
            chain.proceed(request)
        }
    }

    fun <S> createService(serviceClass: Class<S>?): S {
        return retrofit.create(serviceClass)
    }

    companion object {
        private var retrofitConfig: RetrofitConfig? = null
        val instance: RetrofitConfig?
            get() {
                if (retrofitConfig == null) {
                    retrofitConfig = RetrofitConfig()
                }
                return retrofitConfig
            }
    }

    init {
        gson = GsonBuilder().setLenient().create()
        retrofit = createRetrofit()
    }
}
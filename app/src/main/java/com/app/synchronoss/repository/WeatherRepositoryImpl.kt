package com.app.synchronoss.repository

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.app.synchronoss.model.WeatherDataModel
import com.app.synchronoss.retrofit.RetrofitAPIInterface
import com.app.synchronoss.retrofit.RetrofitConfig
import io.reactivex.Observable
import kotlin.math.absoluteValue

private const val APP_ID = "e98fc390c6b8033339df00b82adfbe4e"
class WeatherRepositoryImpl(private val application: Application) : WeatherRepository {
    private val TAG = WeatherRepositoryImpl::class.java.simpleName
    private val DATA_KEY = "weatherData"
    private val LAST_TIME_KEY = "last_time_saved"
    private var apiInterface: RetrofitAPIInterface? = RetrofitConfig.instance?.createService(RetrofitAPIInterface::class.java)

    override fun saveWeatherData(data: String?) {
        val preferences: SharedPreferences = application.getSharedPreferences(TAG, Context.MODE_PRIVATE)
        preferences.edit().putString(DATA_KEY, data).apply()

    }

    override fun deleteWeatherData() {
        val preferences: SharedPreferences = application.getSharedPreferences(TAG, Context.MODE_PRIVATE)
        preferences.edit().clear().apply()
    }

    override fun getSavedWeatherData(): String? {
        val preferences: SharedPreferences = application.getSharedPreferences(TAG, Context.MODE_PRIVATE)
        return preferences.getString(DATA_KEY, "")
    }

    override fun saveWeatherTimeLastSync(time: Long) {
        val preferences: SharedPreferences = application.getSharedPreferences(TAG, Context.MODE_PRIVATE)
        preferences.edit().putLong(LAST_TIME_KEY, time).apply()
    }

    override fun getWeatherTimeLastSync(): Long {
        val preferences: SharedPreferences = application.getSharedPreferences(TAG, Context.MODE_PRIVATE)
        return preferences.getLong(LAST_TIME_KEY, 0)
    }

    override fun getWeatherData(lat: Double, lon: Double): Observable<WeatherDataModel>? {
        return apiInterface?.getCurrentWeather(lat, lon, APP_ID)
    }

}
package com.app.synchronoss.repository

import android.app.Application
import com.app.synchronoss.model.WeatherDataModel
import io.reactivex.Observable

interface WeatherRepository {
    fun saveWeatherData(data: String?)
    fun deleteWeatherData()
    fun getSavedWeatherData(): String?
    fun saveWeatherTimeLastSync(time: Long)
    fun getWeatherTimeLastSync(): Long
    fun getWeatherData( lat: Double, lon: Double): Observable<WeatherDataModel>?
}
package com.app.synchronoss.utility

import android.content.Context
import android.net.ConnectivityManager
import java.text.SimpleDateFormat
import java.util.*

object Utility {
    //    Check Internet Connection
    fun isNetworkConnected(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return cm.activeNetworkInfo != null
    }

    /**
     * Pattern: HH:mm:ss
     */
    fun Date.formatToServerTimeDefaults(): String{
        val sdf= SimpleDateFormat("HH:mm:ss", Locale.getDefault())
        return sdf.format(this)
    }

//    https://api.openweathermap.org/data/2.5/weather?lat=35&lon=139&appid=0ae2c297304879bae1d091104c3cf5e2

    fun String?.formatToTwoDecimal(): String {
        val amount = this?.toDoubleOrNull()
        amount?.let {
            val twoDecimalAmount = String.format("%.2f", it).toDouble()
            return "$twoDecimalAmount"
        }
        return this ?: ""
    }
}